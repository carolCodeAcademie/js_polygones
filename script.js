// créer mon objet classe Hexagone :
		var canvas = document.getElementById("myCanvas");
		var context = canvas.getContext("2d");
		context.beginPath();
		context.translate(canvas.width/2, canvas.height/2);




// créer mon objet classe Hexagone :
class Hex {
	constructor(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	to_axial() {
	    var q = this.x
	    var r = this.z
	    return new Point(q, r)
	}

	checkHex() {
		var result;
		var total = this.x + this.y + this.z;
			if (total == 0) {
				result = true;
			}
			else {
				result = false;
			}
		return(result)
	}
}


// appeler instancier la classe 
// var h1 = new Hex(5, -5, 0);
// var h2 = new Hex(1, 1, 0);
// var h3 = new Hex(-3, -2, 5);





// créer mon objet classe Point :
class Point {
	constructor(q, r) {
		this.q = q;
		this.r = r;
	}

	to_cube() {
		var x = this.q
		var z = this.r
		var y = -x -z
		return new Hex(x, y, z)
	}



	// calcule les coordonnées d'un point
	pointy_hex_corner(center, size, i) {

		var angle_deg = 60 * i - 30
	    var angle_rad = Math.PI / 180 * angle_deg
		var decimalQ = (center.q + size * Math.cos(angle_rad)).toFixed(2) - Math.floor(center.q + size * Math.cos(angle_rad))
		var decimalR = (center.r + size * Math.sin(angle_rad)).toFixed(2) - Math.floor(center.r + size * Math.sin(angle_rad))
		var entierQ = (center.q + size * Math.cos(angle_rad)).toFixed(2)
		var entierR = (center.r + size * Math.sin(angle_rad)).toFixed(2)
			if (decimalQ > 0.98 && decimalQ <= 0.99) {
				Math.round(entierQ);
			}
			if (decimalR > 0.98 && decimalR <= 0.99) {
				Math.round(entierR);
			}
		// console.log(entierQ)
	    return new Point(entierQ, entierR)
	}

	// trouve les points des 6 coordonnées
	findPoints(point) {
		var allCenterPoints = [];
		for (var i = 0; i < 6; i++) {
			allCenterPoints.push(this.pointy_hex_corner(center, size, i));
		}
		return(allCenterPoints);
	}


	// calcule les coordonnées d'un point
	pointy_hex_corner_Neighbours(center, size, i) {
		var angle_deg = 60 * i
	    var angle_rad = Math.PI / 180 * angle_deg
	    return new Point(center.q + size * Math.cos(angle_rad),
	                 center.r + size * Math.sin(angle_rad))
	}

	// trouve les points des 6 coordonnées
	findPointsNeighbours(point) {
		var allPointsNeighbours = [];
		for (var i = 0; i < 6; i++) {
			allPointsNeighbours.push(this.pointy_hex_corner_Neighbours(center, w, i));
		}
		return(allPointsNeighbours);
	}


	// génère (dessine) l'hexagone
	 generateHex () {
		var allCenterPoints = this.findPoints();
		var allPointsNeighbours =this.findPointsNeighbours();
		var tempArray = [];
/*		console.log(allPointsNeighbours)
*/		for (var i = 0; i < 6; i++) {
			for(var j = 0; j < 6; j++) {
			tempArray.push(this.pointy_hex_corner(allPointsNeighbours[i], size, j))
			allHexCorners.push(this.pointy_hex_corner(allPointsNeighbours[i], size, j))
			}
			allHexNeighbours.push(tempArray);
			tempArray = []
		}
/*		console.log("allHexNeighbours", allHexNeighbours)
*/
		context.moveTo(allCenterPoints[0].q, allCenterPoints[0].r);
		for (var i = 1; i < 6; i++) {
			context.lineTo(allCenterPoints[i].q, allCenterPoints[i].r);
		}
		context.lineTo(allCenterPoints[0].q, allCenterPoints[0].r);
/*		context.strokeStyle = 'red'
*/		context.stroke();

		for (var i = 0; i < allPointsNeighbours.length; i++) {
			context.moveTo(allHexNeighbours[i][0].q, allHexNeighbours[i][0].r)
			for (var j = 0; j < 6; j++) {
				context.lineTo(allHexNeighbours[i][j].q, allHexNeighbours[i][j].r);
			}
			context.lineTo(allHexNeighbours[i][0].q, allHexNeighbours[i][0].r);
		}
/*		context.strokeStyle = 'yellow'
*/		context.stroke();
	}

	// filtrer les corners pour suppimer les doublons
	filterHexCorners() {
		this.generateHex()
		for (var i = 0; i < allHexCorners.length; i++) {
			for (var j = 0; j < neighboursPointCircle.length; j++) {
				if (allHexCorners[i].q == neighboursPointCircle[j].q && allHexCorners[i].r == neighboursPointCircle[j].r) {
					nbr++
				}
			}
			if (nbr == 0) {
				neighboursPointCircle.push(allHexCorners[i])
			}
			nbr = 0
		}
/*		console.log("neighboursPointCircle", neighboursPointCircle)
*/	}

	drawCircles() {
		this.filterHexCorners()
		for (var i = 0; i < neighboursPointCircle.length; i++) {
			context.moveTo(neighboursPointCircle[i].q + 10, neighboursPointCircle[i].r + 5)
			context.beginPath();
/*			console.log('moveTo', (neighboursPointCircle[i].q), neighboursPointCircle[i].r)
*/			context.arc(neighboursPointCircle[i].q, neighboursPointCircle[i].r, 10, 0, 2*Math.PI)
/*			console.log(neighboursPointCircle[i].q, neighboursPointCircle[i].r)
			if (i>=0) {*/
			context.strokeStyle = 'red';
			context.stroke()
			context.fillStyle = 'red';
			context.fill();
/*			}
			else {
				context.strokeStyle = 'red';
				context.stroke()
			}	*/
		}
	}




}





/*context.arc(30,50,10,0,2*Math.PI);
context.stroke();
*/



var nbr = 0;
var allHexNeighbours = [];
var allHexCorners = [];
var neighboursPointCircle = [];
/*var test = new Point(260.81, -49.00)
*/




size = 100;


var w = Math.sqrt(3) * size
var h = 2 * size
var p1 = new Point(0, 0)
/*var p2 = new Point(w, h)
*/

center = p1


/*p1.to_cube()
p1.filterHexCorners()
p3.generateHex()*/
p1.drawCircles()


/*function generateNeighbours() {
	// formule size à calculer


w = Math.sqrt(3) * size 
h = 2 * size

	var p1 = new Point(0, 0)
	var neighbours = [];
	for (i = 1; i < 7; i++) {
	 	p[i] = new Point(p["q"] + 1, p["r"])
	 	neighbours.push(p[i])
	}
	return(neighbours)
}
	 	*/



//var g = generateHex()

/*var axial_directions = [
    Hex(+1, 0), Hex(+1, -1), Hex(0, -1), 
    Hex(-1, 0), Hex(-1, +1), Hex(0, +1), 
]

function hex_direction(direction) {
    console.log(hex_direction)
    return axial_directions[direction]
}

function hex_neighbor(hex, direction) {
    var dir = hex_direction(direction)
    return new Hex(hex.q + dir.q, hex.r + dir.r)
}

p1.hex_neighbor()
console.log(hex_direction())*/


// width w = sqrt(3) * size and height h = 2 * size





// tests perso pour faire une boucle qui additionne tous les p
/*var allPs = [p1, p2, p3]
console.log(allPs);

function checkAllHex() {
	for (i = 0; i < allPs; i++) {
		i.addHex();
		console.log(result[i]);
	}
console.log(p1.chcckHex());
checkAllHex();
*/


/* autrer syntaxe pour classe Point
class Point{
    constructor(q,r){
        this.q = q;
    this.r = r;
    }

    toHex(){
        return new Hex(this.q, -this.q-this.r, this.r);
    }
}


var p1 = new Point(4, -4);

var p5 = pointy_hex_corner(p1, 100, 0);
/*
function findPoints(point) {
	var allCenterPoints = [];
	var size = 100;
	for (i = 0; i < 0; i++) {
		pointy_hex_corner(p1);
		allCenterPoints = allCenterPoints + newPoint();
	}
	console.log(allCenterPoints)
}















*/

